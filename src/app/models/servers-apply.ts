export class Server {

    constructor(public ip?: string,
                public port?: number,
                public name?: string) { }
}