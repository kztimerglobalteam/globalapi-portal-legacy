export class Server {
    
    constructor(public id?:number,
                public api_key?:string,
                public port?:number,
                public ip?:string,
                public name?:string,
                public owner_steamid64?:string,
                public created_on?:string,
                public approval_status?:number) { }
}