import { Component } from '@angular/core';
import { LoginService } from '../../services/login-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html'
})

export class AppHeaderComponent {
  
  constructor(private loginService: LoginService) { }

  public profileIcon = localStorage.getItem("ProfileIcon");
  public steamid64 = localStorage.getItem("SteamID64");
  public role = this.loginService.isGlobalAdmin() ? "Admin" : "User";

  reload() {
    location.reload();
  }
  
  logout() {
    this.loginService.logout();
  }
}
