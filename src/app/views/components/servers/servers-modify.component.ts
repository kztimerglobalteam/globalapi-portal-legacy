import { Component, OnInit } from '@angular/core';
import { ServersService } from '../../../services/servers-service.service';
import { NotificationService } from '../../../services/notification.service';

import { Server } from '../../../models/servers-modify';

@Component({
  templateUrl: 'servers-modify.component.html'
})

export class ServersModifyComponent implements OnInit {

  constructor(private serversService: ServersService,
              private notificationService: NotificationService) {}

  public currentPage:number;
  public oldData:ArrayBuffer[] = [];
  public serversData:ArrayBuffer[] = [];

  public title = "Are you sure";
  public modifymsg = "You want to modify this server?";
  public revertmsg = "You want to revert changes made to this server?";

  ngOnInit() {
    this.subscribeToServersData();
  }

  dataModified(index):boolean {
    return JSON.stringify(this.serversData[index]) !== JSON.stringify(this.oldData[index]);
  }

  modify(id:number) {
    let index = this.serversData.findIndex(x => x['id'] == id);

    if (this.dataModified(index)) {

      let data = this.serversData[index];
      let api_key = data['api_key'];
      let name = data['name'];
      let ip = data['ip'];
      let port = data['port'];
      let owner_sid64 = data['owner_steamid64'];
      let status = data['approval_status'];

      this.subscribeToModifyData(id, api_key, name, ip, port, owner_sid64, status);
    }
  }

  revert(id:number) {
    let index = this.serversData.findIndex(x => x['id'] == id);
    this.serversData[index] = this.oldData[index];
    this.notificationService.showSuccess("Servers", "Reverted the changes to the server");
  }

  subscribeToServersData() {
    this.serversService.getServers().subscribe(
    (data) => this.pushServersData(data),
    (err) => this.errorHandle(err));
  }

  subscribeToModifyData(id:number, api_key?: string, name?: string, ip?:string, port?: number, owner_sid64?:string, status?: number) {
    this.serversService.modifyServer(id, api_key, name, ip, port, owner_sid64, status).subscribe(
    (data) => this.pushModifyData(data),
    (err) => this.errorHandle(err));
  }

  pushServersData(data) {
    let pattern = /\"owner_steamid64\"\:([0-9]{17})\,/g;
    data = data.replace(pattern, '"owner_steamid64":"$1",');

    this.serversData = JSON.parse(data);                            // This is the two-way data binded data (changes)
    this.oldData = this.serversData.map(x => Object.assign({}, x)); // copy of unmodified server data
  }

  pushModifyData(data) {
    this.notificationService.showSuccess("Servers", "Successfully modified the server!");
    this.subscribeToServersData();
  }

  errorHandle(err) {
    console.log(err);
    this.notificationService.showError("Servers", err.status + " " + err.statusText);
  }

}
