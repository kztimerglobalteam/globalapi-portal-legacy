import { Component } from '@angular/core';

@Component({
  templateUrl: 'records-modify.component.html'
})

export class RecordsModifyComponent {

 public inputDisabled1:boolean = true;
 public inputDisabled2:boolean = true;
 public inputDisabled3:boolean = true;
 public inputDisabled4:boolean = true;
 public inputDisabled5:boolean = true;
 public inputDisabled6:boolean = true;
 public inputDisabled7:boolean = true;
 public inputDisabled8:boolean = true;

 disableInput(input:number) {
   switch(input) {
     case 1: this.inputDisabled1 = !this.inputDisabled1; break;
     case 2: this.inputDisabled2 = !this.inputDisabled2; break;
     case 3: this.inputDisabled3 = !this.inputDisabled3; break;
     case 4: this.inputDisabled4 = !this.inputDisabled4; break;
     case 5: this.inputDisabled5 = !this.inputDisabled5; break;
     case 6: this.inputDisabled6 = !this.inputDisabled6; break;
     case 7: this.inputDisabled7 = !this.inputDisabled7; break;
     case 8: this.inputDisabled8 = !this.inputDisabled8; break;
   }
 }

  constructor() { }
}
