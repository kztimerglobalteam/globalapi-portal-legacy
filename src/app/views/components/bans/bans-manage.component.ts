import { Component } from '@angular/core';
import { BansService } from '../../../services/bans-service.service';
import { NotificationService } from '../../../services/notification.service';

@Component({
  templateUrl: 'bans-manage.component.html'
})
export class BansManageComponent {

  constructor(private bansService: BansService,
              private notificationService: NotificationService) { }


  public title = "Are you sure";
  public modifymsg = "You want to apply changes made to this ban?";
  public revertmsg = "You want to revert changes made to this ban?";

  public currentPage:number;
  public oldData:ArrayBuffer[] = [];
  public bansData:ArrayBuffer[] = [];

  ngOnInit() {
    this.bansService.banIsExpired = false;
    this.subscribeToBansData();
  }

  unban(id:number) {

    let index = this.bansData.findIndex(x => x['id'] == id);
    let unixtime = new Date().getTime();

    let temp = new Date(unixtime).toISOString();
    //this.bansData[index]['expires_on'] = temp;

    this.modify(id);
  }

  printBanLength(datetime:string) {
    if ((datetime.substr(0,4)) == "9999") {
      return "PERMANENT";
    }
    else {
      let time = datetime.substr(11,10);
      let date = datetime.substr(0,10);
      return date + " (" + time + ")";
    }
  }

  dataModified(index):boolean {
    return JSON.stringify(this.bansData[index]) !== JSON.stringify(this.oldData[index]);
  }

  modify(id:number) {

    let index = this.bansData.findIndex(x => x['id'] == id);
    console.log("Unimplemented for now");

    /*if (this.dataModified(index)) {
      let data = this.bansData[index];
      let type = data['ban_type'].toString();
      let ip = data['ip'].toString();
      let steamid64 = data['steamid64'].toString();
      let player_name = data['player_name'].toString();
      let steam_id = data['steam_id'].toString();
      let notes = data['notes'].toString();
      let stats = data['stats'].toString();
      let server_id = parseInt(data['server_id']);
      let expires_on = data['expires_on'].toString();

      this.subscribeToModifyData(id, type, expires_on, ip, steamid64, player_name, steam_id, notes, stats, server_id);
    }*/
  }

  revert(id:number) {

  }

  subscribeToBansData() {
    this.bansService.getBans().subscribe(
    (data) => this.pushBansData(data),
    (err) => this.errorHandle(err));
  }

  subscribeToModifyData(id:number, ban_type:string, expires_on:string, ip:string, steamid64:string, player_name:string, steam_id:string, notes:string, stats:string, server_id:number) {
    this.bansService.modifyBan(id, ban_type, expires_on, ip, steamid64, player_name, steam_id, notes, stats, server_id).subscribe(
    (data) => this.pushModifyData(data),
    (err) => this.errorHandle(err));
  }

  pushBansData(data) {
    let pattern = /\"steamid64\"\:([0-9]{17})\,/g;
    data = data.replace(pattern, '"steamid64":"$1",');

    this.bansData = JSON.parse(data);
    this.oldData = this.bansData.map(x => Object.assign({}, x));
  }

  pushModifyData(data) {
    this.notificationService.showSuccess("Bans", "Successfully modified the ban!");
    this.subscribeToBansData();
  }

  errorHandle(err) {
    this.notificationService.showError("Bans", err.status + " " + err.statusText);
    console.log(err);
  }
}
