import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BansService } from './../../../services/bans-service.service';
import { NotificationService } from '../../../services/notification.service';

@Component({
  templateUrl: 'bans-view.component.html'
})

export class BansViewComponent {

  constructor(private router: Router,
              private banService: BansService,
              private notificationService: NotificationService) { }

  public ready:boolean;
  public selfRoute:boolean;
  public currentPage:number;
  public bansData:ArrayBuffer[] = [];
  public steamid = localStorage.getItem("SteamID64");

  ngOnInit() {
    if (this.router.url.endsWith("/me")) {
      this.selfRoute = true;
      this.subscribeSelfToData();
    }

    else {
      this.selfRoute = false;
      this.subscribeToData();
    }
  }

  subscribeToData() {
    this.banService.getBans().subscribe(
    (data) => this.pushData(data),
    (err) => this.errorFunction(err));
  }

  subscribeSelfToData() {
    this.banService.getBans(this.steamid).subscribe(
    (data) => this.pushData(data),
    (err) => this.errorFunction(err));
  }

  pushData(data) {
    let pattern = /\"steamid64\"\:([0-9]{17})\,/g;
    data = data.replace(pattern, '"steamid64":"$1",');

    this.bansData = JSON.parse(data);
    this.ready = true;
  }

  errorFunction(err) {
    this.notificationService.showError("Bans", err.status + " " + err.statusText);
  }

  formatDate(datetime:string) {
    if ((datetime.substr(0,4)) == "9999") {
      return "PERMANENT";
    }
    else {
      let time = datetime.substr(11,10);
      let date = datetime.substr(0,10);
      return date + " (" + time + ")";
    }
  }

  copySuccess() {
    this.notificationService.showSuccess("Stats", "Succesfully copied to clipboard");
  }
}
