import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from '../../directives/directives.module';

// Pagination & Confirmations
import { NgxPaginationModule } from 'ngx-pagination';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';

// Routing & Service
import { MapsService } from '../../services/maps-service.service';
import { MapsRoutingModule } from './maps-routing.module';

import { MapsViewComponent } from './maps/maps-view.component';
import { MapsManageComponent } from './maps/maps-manage.component';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    DirectivesModule,
    MapsRoutingModule,
    NgxPaginationModule,
    ConfirmationPopoverModule.forRoot()
  ],
  declarations: [
    MapsViewComponent,
    MapsManageComponent
   ],
   providers: [
    MapsService
   ]
})

export class MapsModule { }
