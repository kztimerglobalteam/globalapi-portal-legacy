import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BansViewComponent } from './bans/bans-view.component';
import { BansManageComponent } from './bans/bans-manage.component';

import { AdminAuthGuard } from '../../guards/admin-auth.guard';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Bans'
    },
    children: [
      {
        path: 'manage',
        component: BansManageComponent,
        canActivate: [AdminAuthGuard],
        data: {
          title: 'Manage'
        }
      },
      {
        path: 'view',
        component: BansViewComponent,
        data: {
          title: 'View'
        }
      },
      {
        path: 'view/me',
        component: BansViewComponent,
        data: {
          title: 'View'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BansRoutingModule {}
