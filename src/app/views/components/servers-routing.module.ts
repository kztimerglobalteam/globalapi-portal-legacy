import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ServersApplyComponent } from './servers/servers-apply.component';
import { ServersManageComponent } from './servers/servers-manage.component';
import { ServersAddComponent } from './servers/servers-add.component';
import { ServersModifyComponent } from './servers/servers-modify.component';
import { ServersViewComponent } from './servers/servers-view.component';

import { AdminAuthGuard } from '../../guards/admin-auth.guard';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Servers'
    },
    children: [
      {
        path: 'apply',
        component: ServersApplyComponent,
        data: {
          title: 'Apply'
        }
      },
      {
        path: 'view',
        component: ServersViewComponent,
        data: {
          title: 'View'
        }
      },
      {
        path: 'add',
        component: ServersAddComponent,
        canActivate: [AdminAuthGuard],
        data: {
          title: 'Add'
        }
      },
      {
        path: 'modify',
        component: ServersModifyComponent,
        canActivate: [AdminAuthGuard],
        data: {
          title: 'Modify'
        }
      },
      {
        path: 'manage',
        component: ServersManageComponent,
        canActivate: [AdminAuthGuard],
        data: {
          title: 'Manage'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ServersRoutingModule {}
