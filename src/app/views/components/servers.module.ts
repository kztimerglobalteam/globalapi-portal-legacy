import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DirectivesModule } from '../../directives/directives.module';

// Pagination, Clipboard, Tooltip, Forms & Confirmations
import { NgxPaginationModule } from 'ngx-pagination';
import { ClipboardModule } from 'ngx-clipboard';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';

// Routing & Service
import { ServersService } from '../../services/servers-service.service';
import { ServersRoutingModule } from './servers-routing.module';

import { ServersApplyComponent } from './servers/servers-apply.component';
import { ServersAddComponent } from './servers/servers-add.component';
import { ServersModifyComponent } from './servers/servers-modify.component';
import { ServersManageComponent } from './servers/servers-manage.component';
import { ServersViewComponent } from './servers/servers-view.component';


@NgModule({
  imports: [
    FormsModule, 
    CommonModule,
    ClipboardModule,
    DirectivesModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    ServersRoutingModule,
    TooltipModule.forRoot(),
    ConfirmationPopoverModule.forRoot()
  ],
  declarations: [
    ServersAddComponent,
    ServersViewComponent,
    ServersApplyComponent,
    ServersModifyComponent,
    ServersManageComponent
  ],
  providers: [
    ServersService
  ]
})

export class ServersModule { }
