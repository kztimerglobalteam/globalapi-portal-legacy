import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DirectivesModule } from '../../directives/directives.module';

// Misc
import { NgxPaginationModule } from 'ngx-pagination';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';

// Routing & Service
import { ModesRoutingModule } from './modes-routing.module';
import { ModesService } from '../../services/modes-service.service';

import { ModesModifyComponent } from './modes/modes-modify.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    ModesRoutingModule,
    NgxPaginationModule,
    ConfirmationPopoverModule.forRoot()
  ],
  declarations: [ 
    ModesModifyComponent
  ],
  providers: [
    ModesService
  ]
})

export class ModesModule { }
