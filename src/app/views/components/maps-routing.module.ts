import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapsViewComponent } from './maps/maps-view.component';
import { MapsManageComponent } from './maps/maps-manage.component';

import { AdminAuthGuard } from '../../guards/admin-auth.guard';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Maps'
    },
    children: [
      {
        path: 'manage',
        component: MapsManageComponent,
        canActivate: [AdminAuthGuard],
        data: {
          title: 'Manage'
        }
      },
      {
        path: 'view',
        component: MapsViewComponent,
        data: {
          title: 'View'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapsRoutingModule {}
