import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Pagination
import { NgxPaginationModule } from 'ngx-pagination';

// Routing & Service
import { RecordsRoutingModule } from './records-routing.module';
import { RecordsService } from '../../services/records-service.service';

import { RecordsAddComponent } from './records/records-add.component';
import { RecordsDeleteComponent } from './records/records-delete.component';
import { RecordsModifyComponent } from './records/records-modify.component'; 
import { RecordsViewComponent } from './records/records-view.component';

@NgModule({
  imports: [
    CommonModule,
    NgxPaginationModule,
    RecordsRoutingModule
  ],
  declarations: [
    RecordsAddComponent,
    RecordsViewComponent,
    RecordsDeleteComponent,
    RecordsModifyComponent
   ],
   providers: [
    RecordsService
   ]
})

export class RecordsModule { }
