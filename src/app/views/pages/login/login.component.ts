import { Component, OnInit } from '@angular/core';
import { LoginService } from './../../../services/login-service.service';

@Component({
  templateUrl: 'login.component.html',
  styles: ['::ng-deep body { background-color: #e4e5e6; }']
})

export class LoginComponent implements OnInit {

  constructor(private loginService: LoginService) { }

  public ready:boolean;
  public loggedIn: boolean;

  ngOnInit() {
    this.ready = false;
    this.loginService.login().subscribe(user => {
      this.loginService.setUser(user);
      if (this.loginService.isLoggedIn()) {
        this.loggedIn = true;
        this.loginService.userLoggedIn();
      }
      else {
        this.loggedIn = false;
      }
      this.ready = true;
    });
  }

  public accessDenied:boolean = this.loginService.accessDenied;

  public steamLogin() {
    this.loginService.steamLogin();
  }
}
