import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  templateUrl: '404.component.html'
})

export class Page404Component implements OnInit {

  constructor(private title: Title,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.title.setTitle("Global API - " + this.route.snapshot.data['title']);
  }
}
