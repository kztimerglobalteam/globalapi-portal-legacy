import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router,
         CanActivate, 
         ActivatedRouteSnapshot, 
         RouterStateSnapshot } from '@angular/router';

import 'rxjs/add/operator/map';

import { LoginService } from '../services/login-service.service';

@Injectable()
export class UserAuthGuard implements CanActivate {

  constructor (private router: Router, 
               private loginService: LoginService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    return this.loginService.login().map(user => {
      this.loginService.setUser(user)
      if (this.loginService.isLoggedIn()) {
        this.loginService.userActiveSession();
        return true;
      }
      else { 
        this.loginService.userNotLoggedIn(state.url); 
        return false;
      }
    });
  }
}
