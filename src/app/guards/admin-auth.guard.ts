import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router,
         CanActivate, 
         ActivatedRouteSnapshot, 
         RouterStateSnapshot } from '@angular/router'; 

import { LoginService } from '../services/login-service.service';

@Injectable()
export class AdminAuthGuard implements CanActivate {

  constructor (private router: Router,
               private loginService: LoginService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (this.loginService.isGlobalAdmin()) {
      return true;
    }
    else {
      this.router.navigate(['/portal']);
      return false;
    }
  }
}
