import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Layout
import { FullLayoutComponent } from './containers/full-layout/full-layout.component';

import { Page404Component } from './views/pages/404/404.component';
import { LoginComponent } from './views/pages/login/login.component';

import { UserAuthGuard } from './guards/user-auth.guard';
import { AdminAuthGuard } from './guards/admin-auth.guard';

export const routes: Routes = [
  {
    path: '',
    component: FullLayoutComponent,
    canActivate: [UserAuthGuard],
    data: {
      title: 'Portal'
    },
    children: [
    {
      path: '',
      loadChildren: './views/dashboard/dashboard.module#DashboardModule',
    },
    {
      path: 'bans',
      loadChildren: './views/components/bans.module#BansModule'
    },
    {
      path: 'maps',
      loadChildren: './views/components/maps.module#MapsModule'
    },
    {
      path: 'modes',
      loadChildren: './views/components/modes.module#ModesModule'
    },
    {
      path: 'records',
      loadChildren: './views/components/records.module#RecordsModule'
    },
    {
      path: 'servers',
      loadChildren: './views/components/servers.module#ServersModule'
    }]
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login'
    }
  },
  {
    path: '**',
    component: Page404Component,
    data: {
      title: '404'
    }
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
  providers: [ UserAuthGuard,
               AdminAuthGuard ]
})

export class AppRoutingModule { }
