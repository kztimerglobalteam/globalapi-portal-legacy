import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ModesService {

  constructor(private http: HttpClient) { }

  private baseUrl:string = "https://auth.global-api.com/api/v1/modes";

  getModes() {
    let requestUrl:string = this.baseUrl;

    return this.http.get(requestUrl, { withCredentials: true, responseType: 'text' });
  }

  modifyMode(id:number, name:string, description:string, latest_ver:string, latest_ver_desc:string, website:string, repo: string, contact_steamid64:string) {
    let requestUrl:string = this.baseUrl + "/" + id;
    let data = {"id": id, "name": name, "description": description, "latest_version": latest_ver, "latest_version_description": latest_ver_desc, "website": website, "repo": repo, "contact_steamid64": contact_steamid64};

    return this.http.put(requestUrl, data, { withCredentials: true});
  }
}
