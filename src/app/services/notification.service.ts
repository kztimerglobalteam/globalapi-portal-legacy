import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class NotificationService {

  constructor(private toastrService: ToastrService) {}

  showSuccess(title, msg) {
    this.toastrService.success(msg, title);
  }

  showError(title, msg) {
    this.toastrService.error(msg, title);
  }

  showWarning(title, msg) {
    this.toastrService.warning(msg, title);
  }

  showInfo(title, msg) {
    this.toastrService.info(msg, title);
  }
}
