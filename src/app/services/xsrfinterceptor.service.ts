import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpEvent,
         HttpRequest,
         HttpHandler,
         HttpInterceptor,  
         HttpXsrfTokenExtractor } from '@angular/common/http';

@Injectable()
export class XSRFInterceptor implements HttpInterceptor {

  constructor(private tokenExtractor: HttpXsrfTokenExtractor) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    if (req.url.startsWith("http://auth.global-api.com") || req.url.startsWith("https://auth.global-api.com"))
    {
      var requestToForward = req;
      let token = this.tokenExtractor.getToken() as string;

      if (token !== null) {
        requestToForward = req.clone({ setHeaders: { "X-XSRF-TOKEN": token } });
      }
      return next.handle(requestToForward);
    }
    return next.handle(req);
  }
}
